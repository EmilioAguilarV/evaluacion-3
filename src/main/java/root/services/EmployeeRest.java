/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.EmployeeJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.entities.Employee;

/**
 *
 * @author Mclio
 */
    @Path("/EmployeeRest")
public class EmployeeRest {





    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {

  
        EmployeeJpaController dao = new EmployeeJpaController();
        List<Employee> lista = dao.findEmployeeEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();

    }
    
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") int idbuscar){
            EmployeeJpaController dao =new EmployeeJpaController();
     Employee  sel=dao.findEmployee(idbuscar);
        return Response.ok(200).entity(sel).build();
    }
    
    
   @POST
   @Produces(MediaType.APPLICATION_JSON)
    public Response nuevo(Employee employee){
   System.out.println("nuevo seleccion.getNombre():" + employee.getName());
    System.out.println("nuevo seleccion.getCodigo():" + employee.getId());     
   
   EmployeeJpaController dao =new EmployeeJpaController();
        try {
            dao.create(employee);
        } catch (Exception ex) {
            Logger.getLogger(EmployeeRest.class.getName()).log(Level.SEVERE, null, ex);
            
        }
    return Response.ok(200).entity(employee).build();
     
    }
    
    @PUT
    public Response actualizar(Employee seleccion){
          EmployeeJpaController dao =new EmployeeJpaController();
        try {
            dao.edit(seleccion);
        } catch (Exception ex) {
            Logger.getLogger(EmployeeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
          
          
       return Response.ok(200).entity(seleccion).build();
     

    }
    
   @DELETE
   @Path("/{iddelete}")
   @Produces(MediaType.APPLICATION_JSON)
      public Response eliminaId(@PathParam("iddelete") int iddelete){
          System.out.println("eliminaId :" + iddelete);
            EmployeeJpaController dao =new EmployeeJpaController();
        try {
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(EmployeeRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       return Response.ok("cliente eliminado").build();
     

    }  
    
  
  
}  

    
    

